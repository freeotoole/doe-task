# Front-end Developer Practical Task
As I don't have demonstrable commercial experience utilising modern JavaScript frameworks I decided to use Vue and Bootstrap 4 for this task.

Usually I'd start building a static html version of the design and then convert that to the chosen front end framework or back end templating engine (eg, Apache FreeMarker). However, with limited time for this task I thought I'd focus on tooling and functionality in Vue before the UI.

## Thing I'd change if I had more time
Obviously the basic design I didn't get time for - sizing, alignment, icons, using transform: skew for the tile slant etc.

I haven't got to the modal. I would start by trying the bootstrap modal rather than a lightbox as the large photos in the demo don't appear to be linked in an album.

I'd like to limit the API response and update returned entries as needed.

Usually I'd componentise the Vue app and scope the scss to each component.


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
